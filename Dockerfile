FROM inspectorio/python:3.6

MAINTAINER Ihar Ivanou <ihar@inspio.com>

RUN pip install --no-cache-dir -r requirements.txt
