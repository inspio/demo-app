from flask import Flask, request
import boto3, json

app = Flask(__name__)

@app.route('/healz', methods=['GET'])
def healz():
    data={"status": "OK"}
    status_code=200

    try:
        sts = boto3.client('sts')
        arn = sts.get_caller_identity()['Arn']
    except Exception as e:
        data["status"] = "SOS"
        status_code = 503
    return json.dumps(data), status_code

@app.route("/ec2", methods=['POST'])
def get_ec2_status():
    result = dict()
    status_code = 200
    try:
        req_data = request.get_json()
        region = req_data["region"]
        state = req_data["state"]

        ec2 = boto3.client('ec2', region_name=region)
        instances = ec2.describe_instance_status(
            Filters=[
                {
                    'Name': 'instance-state-name',
                    'Values': [state]
                }
            ])

        for instance in instances["InstanceStatuses"]:
            result[instance['InstanceId']]=instance['InstanceState']['Name']
    except Exception as e:
        status_code = 500
    return json.dumps(result), status_code


if __name__ == "__main__":
    app.run(host='0.0.0.0', debug=True)